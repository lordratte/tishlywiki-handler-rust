use std::process::{Command, Stdio};
use std::net::{TcpListener, TcpStream, Shutdown};
use std::os::unix::io::{AsRawFd, FromRawFd};
use std::env;


fn handle_client(stream: TcpStream) {
    let fname = env::var("o_fname").unwrap();
    let fd_in = stream.as_raw_fd() ;
    let fd_out = stream.as_raw_fd() ;
    let mut proc = Command::new("bash")
        .arg(fname)
        .stdin(unsafe { Stdio::from_raw_fd(fd_in) })
        .stdout(unsafe { Stdio::from_raw_fd(fd_out) })
        .stderr(Stdio::inherit())
        .envs(env::vars())
        .spawn()
        .unwrap();

    proc.wait();
    stream.shutdown(Shutdown::Both);
}

fn main() -> std::io::Result<()> {
    let listener = TcpListener::bind([env::var("o_host").unwrap(),env::var("o_port").unwrap()].join(":")).unwrap();
    loop {
        for stream in listener.incoming() {
            handle_client(stream?);
        }
    }
}
