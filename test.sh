#!/bin/bash
mkdir -p builds
gitlab-runner exec docker --docker-volumes "$(realpath builds):/builds" build
